/* SPDX-License-Identifier: GPL-2.0 */

/*
 * NOTE(José): The GPIO portion of this driver is fairly simple to understand.
 * I didn't have much time to understand the intricate details about the integration
 * with the IRQ subsystem though. Most of what has been done here is a copy of what I've
 * seen other I2C IO expander drivers do, in special:
 * 	gpio-max732x.c
 *	gpio-pcf857x.c
 * This means that there might be configurations that are wrong and are just working by
 * pure chance.
 *
 * This driver also considers that this chip will be used as an irq chip. If that is
 * not needed, the configuration and functions related to IRQ may be removed.
 */

#include <linux/irq.h>
#include <linux/interrupt.h>
#include <linux/pinctrl/pinconf-generic.h>
#include <linux/export.h>
#include <linux/gpio/driver.h>
#include <linux/gpio/consumer.h>
#include <linux/regmap.h>
#include <linux/device.h>
#include <linux/errno.h>
#include <linux/i2c.h>
#include <linux/input.h>
#include <linux/module.h>
#include <linux/proc_fs.h>

#define PI4IO16_DRV_NAME "pi4ioe5v6416"
#define PI4IO16_TX_MAX_RETRIES 3

#define PI4IO16_IRQ_CHIP_NAME (PI4IO16_DRV_NAME "-irq-chip")
#define PI4IO16_IRQ_NAME (PI4IO16_DRV_NAME "-irq")

enum {
	PI4IO16_REG_INPUT_0 = 0x00,
	PI4IO16_REG_OUTPUT_0 = 0x02,
	PI4IO16_REG_POLARITY_INVERSION_0 = 0x04,
	PI4IO16_REG_CONFIGURATION_0 = 0x06,
	PI4IO16_REG_OUTPUT_DRIVE_STRENGTH_00 = 0x40,
	PI4IO16_REG_OUTPUT_DRIVE_STRENGTH_10 = 0x42,
	PI4IO16_REG_INPUT_LATCH_0 = 0x44,
	PI4IO16_REG_PULL_UP_PULL_DOWN_ENABLE_0 = 0x46,
	PI4IO16_REG_PULL_UP_PULL_DOWN_SELECT_0 = 0x48,
	PI4IO16_REG_INTERRUPT_MASK_0 = 0x4a,
	PI4IO16_REG_INTERRUPT_STATUS_0 = 0x4c,
	PI4IO16_REG_OUTPUT_PORT_CONFIGURATION = 0x4f,
};

struct pi4io16_data {
	struct i2c_client *client;
	struct gpio_desc *irq_pin;
	struct proc_dir_entry *proc_file;

	struct gpio_chip gpio;

	struct mutex irq_lock;
	struct mutex lock;

	int irq;
	u16 irq_mask_reg;
	u16 cur_irq_mask_reg;
	u16 irq_rising_triggers;
	u16 irq_falling_triggers;
};

static int pi4io16_i2c_write_byte(struct i2c_client *client, u8 reg, u8 byte)
{
	struct i2c_msg msg;
	int retries = 0, ret = 0;
	u8 buffer[2];

	buffer[0] = reg;
	buffer[1] = byte;

	msg.addr = client->addr;
	msg.flags = 0;
	msg.buf = buffer;
	msg.len = sizeof(buffer);

	do {
		ret = i2c_transfer(client->adapter, &msg, 1);
		++retries;
	} while (ret != 1 && retries < PI4IO16_TX_MAX_RETRIES);

	if (ret <= 0)
		ret = -EIO;

	return ret;
}

static int pi4io16_i2c_read_byte(struct i2c_client *client, u8 reg, u8 *byte)
{
	struct i2c_msg msgs[2];
	int retries = 0, ret = 0;

	msgs[0].addr = client->addr;
	msgs[0].flags = 0;
	msgs[0].buf = &reg;
	msgs[0].len = sizeof(reg);

	msgs[1].addr = client->addr;
	msgs[1].flags = I2C_M_RD;
	msgs[1].buf = byte;
	msgs[1].len = sizeof(*byte);

	do {
		ret = i2c_transfer(client->adapter, msgs, 2);
		++retries;
	} while (ret != 2 && retries < PI4IO16_TX_MAX_RETRIES);

	if (ret <= 0)
		ret = -EIO;

	return ret;
}

static int pi4io16_i2c_write_word(struct i2c_client *client, u8 reg, u16 word)
{
	int ret;
	u8 lo, hi;

	lo = word & 0xff;
	hi = word >> 8;

	ret = pi4io16_i2c_write_byte(client, reg, lo);
	if (ret < 0)
		return ret;

	ret = pi4io16_i2c_write_byte(client, reg + 1, hi);
	return ret;
}

static int pi4io16_i2c_read_word(struct i2c_client *client, u8 reg, u16 *word)
{
	int ret;
	u8 lo, hi;

	ret = pi4io16_i2c_read_byte(client, reg, &lo);
	if (ret < 0)
		goto out;

	ret = pi4io16_i2c_read_byte(client, reg + 1, &hi);
	if (ret < 0)
		goto out;

	*word = (hi << 8) | lo;

out:
	return ret;
}

static int pi4io16_i2c_write_word_and_verify(struct i2c_client *client, u8 reg,
					     u16 word)
{
	int ret;
	u16 written;

	ret = pi4io16_i2c_write_word(client, reg, word);
	if (ret < 0)
		goto out;

	ret = pi4io16_i2c_read_word(client, reg, &written);
	if (ret < 0)
		goto out;

	if (written != word)
		ret = -EIO;

out:
	return ret;
}

static void pi4io16_irq_mask(struct irq_data *d)
{
	struct gpio_chip *gc = irq_data_get_irq_chip_data(d);
	struct pi4io16_data *pi4io16 = gpiochip_get_data(gc);
	irq_hw_number_t hwirq = irqd_to_hwirq(d);

	pi4io16->cur_irq_mask_reg |= BIT(hwirq);

	gpiochip_disable_irq(gc, hwirq);
}

static void pi4io16_irq_unmask(struct irq_data *d)
{
	struct gpio_chip *gc = irq_data_get_irq_chip_data(d);
	struct pi4io16_data *pi4io16 = gpiochip_get_data(gc);
	irq_hw_number_t hwirq = irqd_to_hwirq(d);

	gpiochip_enable_irq(gc, hwirq);

	pi4io16->cur_irq_mask_reg &= ~BIT(hwirq);
}

static int pi4io16_irq_set_type(struct irq_data *d, unsigned int type)
{
	struct gpio_chip *gc;
	struct pi4io16_data *pi4io16;
	int ret;
	irq_hw_number_t hwirq;

	gc = irq_data_get_irq_chip_data(d);
	pi4io16 = gpiochip_get_data(gc);
	hwirq = irqd_to_hwirq(d);

	switch (type) {
	case IRQ_TYPE_EDGE_RISING:
	case IRQ_TYPE_EDGE_FALLING:
	case IRQ_TYPE_EDGE_BOTH:
		ret = 0;
		break;
	default:
		ret = -ENOTSUPP;
	}

	if (ret != 0)
		return ret;

	if (type & IRQ_TYPE_EDGE_RISING)
		pi4io16->irq_rising_triggers |= BIT(hwirq);
	else
		pi4io16->irq_rising_triggers &= ~BIT(hwirq);

	if (type & IRQ_TYPE_EDGE_FALLING)
		pi4io16->irq_falling_triggers |= BIT(hwirq);
	else
		pi4io16->irq_falling_triggers &= ~BIT(hwirq);

	return ret;
}

static void pi4io16_irq_update_mask(struct pi4io16_data *pi4io16)
{
	if (pi4io16->irq_mask_reg == pi4io16->cur_irq_mask_reg)
		return;

	pi4io16->irq_mask_reg = pi4io16->cur_irq_mask_reg;

	mutex_lock(&pi4io16->lock);
	pi4io16_i2c_write_word(pi4io16->client, PI4IO16_REG_INTERRUPT_MASK_0,
			       pi4io16->irq_mask_reg);
	/* FIXME(José): Probably place this somewhere else, but I think that latches should be enabled for interrupt pins. */
	pi4io16_i2c_write_word(pi4io16->client, PI4IO16_REG_INPUT_LATCH_0,
			       pi4io16->irq_mask_reg);
	mutex_unlock(&pi4io16->lock);
}

static void pi4io16_irq_bus_lock(struct irq_data *d)
{
	struct gpio_chip *gc = irq_data_get_irq_chip_data(d);
	struct pi4io16_data *pi4io16 = gpiochip_get_data(gc);

	mutex_lock(&pi4io16->irq_lock);
	pi4io16->cur_irq_mask_reg = pi4io16->irq_mask_reg;
}

static void pi4io16_irq_bus_sync_unlock(struct irq_data *d)
{
	struct gpio_chip *gc = irq_data_get_irq_chip_data(d);
	struct pi4io16_data *pi4io16 = gpiochip_get_data(gc);

	pi4io16_irq_update_mask(pi4io16);
	mutex_unlock(&pi4io16->irq_lock);
}

static const struct irq_chip pi4io16_irq_chip = {
	.name = PI4IO16_IRQ_CHIP_NAME,
	.irq_mask = pi4io16_irq_mask,
	.irq_unmask = pi4io16_irq_unmask,
	.irq_bus_lock = pi4io16_irq_bus_lock,
	.irq_bus_sync_unlock = pi4io16_irq_bus_sync_unlock,
	.irq_set_type = pi4io16_irq_set_type,
	.flags = IRQCHIP_IMMUTABLE,
	GPIOCHIP_IRQ_RESOURCE_HELPERS,
};

static int pi4io16_gpio_get_direction(struct gpio_chip *gc, unsigned int offset)
{
	struct pi4io16_data *pi4io16;
	int ret;
	u16 word;

	pi4io16 = gpiochip_get_data(gc);

	mutex_lock(&pi4io16->lock);

	ret = pi4io16_i2c_read_word(pi4io16->client,
				    PI4IO16_REG_CONFIGURATION_0, &word);
	if (ret < 0)
		goto out;

	ret = (word >> offset) & 1;

out:
	mutex_unlock(&pi4io16->lock);
	return ret;
}

static int pi4io16_gpio_get(struct gpio_chip *gc, unsigned int offset)
{
	struct pi4io16_data *pi4io16;
	int ret;
	u16 word;

	pi4io16 = gpiochip_get_data(gc);

	mutex_lock(&pi4io16->lock);

	ret = pi4io16_i2c_read_word(pi4io16->client, PI4IO16_REG_INPUT_0,
				    &word);
	if (ret < 0)
		goto out;

	ret = (word & BIT(offset)) != 0;

out:
	mutex_unlock(&pi4io16->lock);
	return ret;
}

static void pi4io16_gpio_set(struct gpio_chip *gc, unsigned int offset,
			     int value)
{
	struct pi4io16_data *pi4io16;
	int ret;
	u16 word;

	pi4io16 = gpiochip_get_data(gc);

	mutex_lock(&pi4io16->lock);

	ret = pi4io16_i2c_read_word(pi4io16->client, PI4IO16_REG_OUTPUT_0,
				    &word);
	if (ret < 0)
		goto out;

	if (value)
		word |= BIT(offset);
	else
		word &= ~BIT(offset);

	ret = pi4io16_i2c_write_word(pi4io16->client, PI4IO16_REG_OUTPUT_0,
				     word);
	if (ret < 0)
		goto out;

out:
	mutex_unlock(&pi4io16->lock);
}

static int pi4io16_gpio_set_direction(struct gpio_chip *gc, unsigned int offset,
				      int direction)
{
	struct pi4io16_data *pi4io16;
	int ret;
	u16 word;

	pi4io16 = gpiochip_get_data(gc);

	mutex_lock(&pi4io16->lock);

	ret = pi4io16_i2c_read_word(pi4io16->client,
				    PI4IO16_REG_CONFIGURATION_0, &word);
	if (ret < 0)
		goto out;

	if (direction == GPIO_LINE_DIRECTION_IN)
		word |= BIT(offset);
	else
		word &= ~BIT(offset);

	ret = pi4io16_i2c_write_word(pi4io16->client,
				     PI4IO16_REG_CONFIGURATION_0, word);
	if (ret < 0)
		goto out;

	ret = 0;

out:
	mutex_unlock(&pi4io16->lock);
	return ret;
}

static int pi4io16_gpio_direction_input(struct gpio_chip *gc,
					unsigned int offset)
{
	return pi4io16_gpio_set_direction(gc, offset, GPIO_LINE_DIRECTION_IN);
}

static int pi4io16_gpio_direction_output(struct gpio_chip *gc,
					 unsigned int offset, int value)
{
	struct pi4io16_data *pi4io16;
	int ret;

	pi4io16 = gpiochip_get_data(gc);

	ret = pi4io16_gpio_set_direction(gc, offset, GPIO_LINE_DIRECTION_OUT);
	if (ret < 0)
		return ret;

	pi4io16_gpio_set(gc, offset, value);
	return ret;
}

static int pi4io16_gpio_disable_pull(struct gpio_chip *gc, unsigned int offset)
{
	struct pi4io16_data *pi4io16;
	int ret;
	u16 word;

	pi4io16 = gpiochip_get_data(gc);

	mutex_lock(&pi4io16->lock);

	ret = pi4io16_i2c_read_word(
		pi4io16->client, PI4IO16_REG_PULL_UP_PULL_DOWN_ENABLE_0, &word);
	if (ret < 0)
		goto out;

	word &= ~BIT(offset);

	ret = pi4io16_i2c_write_word(
		pi4io16->client, PI4IO16_REG_PULL_UP_PULL_DOWN_ENABLE_0, word);
	if (ret < 0)
		goto out;

	ret = 0;

out:
	mutex_unlock(&pi4io16->lock);
	return ret;
}

static int pi4io16_gpio_apply_pull(struct gpio_chip *gc, unsigned int offset,
				   bool is_pull_up)
{
	struct pi4io16_data *pi4io16;
	int ret;
	u16 word;

	pi4io16 = gpiochip_get_data(gc);

	mutex_lock(&pi4io16->lock);

	ret = pi4io16_i2c_read_word(
		pi4io16->client, PI4IO16_REG_PULL_UP_PULL_DOWN_SELECT_0, &word);
	if (ret < 0)
		goto out;

	if (is_pull_up)
		word |= BIT(offset);
	else
		word &= ~BIT(offset);

	ret = pi4io16_i2c_write_word(
		pi4io16->client, PI4IO16_REG_PULL_UP_PULL_DOWN_SELECT_0, word);
	if (ret < 0)
		goto out;

	ret = pi4io16_i2c_read_word(
		pi4io16->client, PI4IO16_REG_PULL_UP_PULL_DOWN_ENABLE_0, &word);
	if (ret < 0)
		goto out;

	word |= BIT(offset);

	ret = pi4io16_i2c_write_word(
		pi4io16->client, PI4IO16_REG_PULL_UP_PULL_DOWN_ENABLE_0, word);
	if (ret < 0)
		goto out;

	ret = 0;

out:
	mutex_unlock(&pi4io16->lock);
	return ret;
}

static int pi4io16_gpio_set_config(struct gpio_chip *gc, unsigned int offset,
				   unsigned long config)
{
	switch (pinconf_to_config_param(config)) {
	case PIN_CONFIG_BIAS_PULL_UP:
		return pi4io16_gpio_apply_pull(gc, offset, true);
	case PIN_CONFIG_BIAS_PULL_DOWN:
		return pi4io16_gpio_apply_pull(gc, offset, false);
	case PIN_CONFIG_BIAS_DISABLE:
		return pi4io16_gpio_disable_pull(gc, offset);
	default:
		break;
	}

	return -ENOTSUPP;
}

static int pi4io16_gpio_get_multiple(struct gpio_chip *gc, unsigned long *mask,
				     unsigned long *bits)
{
	struct pi4io16_data *pi4io16;
	int ret;
	u16 word;

	pi4io16 = gpiochip_get_data(gc);

	mutex_lock(&pi4io16->lock);

	ret = pi4io16_i2c_read_word(pi4io16->client, PI4IO16_REG_INPUT_0,
				    &word);
	if (ret < 0)
		goto out;

	*bits = word & *mask;
	ret = 0;
out:
	mutex_unlock(&pi4io16->lock);
	return ret;
}

static void pi4io16_gpio_set_multiple(struct gpio_chip *gc, unsigned long *mask,
				      unsigned long *bits)
{
	struct pi4io16_data *pi4io16;
	unsigned long value;
	int ret;
	u16 word;

	pi4io16 = gpiochip_get_data(gc);

	mutex_lock(&pi4io16->lock);

	ret = pi4io16_i2c_read_word(pi4io16->client, PI4IO16_REG_OUTPUT_0,
				    &word);
	if (ret < 0)
		goto out;

	value = word;
	bitmap_replace(&value, &value, bits, mask, gc->ngpio);
	word = value;

	ret = pi4io16_i2c_write_word(pi4io16->client, PI4IO16_REG_OUTPUT_0,
				     word);
	if (ret < 0)
		goto out;

out:
	mutex_unlock(&pi4io16->lock);
}

static int pi4io16_gpio_setup(struct pi4io16_data *pi4io16)
{
	struct device *dev;
	struct gpio_chip *gc;
	struct gpio_irq_chip *gic;
	int ret;

	dev = &pi4io16->client->dev;
	gc = &pi4io16->gpio;

	gc->ngpio = 16;
	gc->label = pi4io16->client->name;
	gc->parent = dev;
	gc->owner = THIS_MODULE;
	gc->base = -1;
	gc->can_sleep = true;

	gc->get_direction = pi4io16_gpio_get_direction;
	gc->direction_input = pi4io16_gpio_direction_input;
	gc->direction_output = pi4io16_gpio_direction_output;
	gc->get = pi4io16_gpio_get;
	gc->get_multiple = pi4io16_gpio_get_multiple;
	gc->set = pi4io16_gpio_set;
	gc->set_multiple = pi4io16_gpio_set_multiple;
	gc->set_config = pi4io16_gpio_set_config;

	gic = &gc->irq;
	gpio_irq_chip_set_chip(gic, &pi4io16_irq_chip);
	gic->parent_handler = NULL;
	gic->num_parents = 0;
	gic->parents = NULL;
	gic->default_type = IRQ_TYPE_NONE;
	gic->handler = handle_bad_irq;
	gic->threaded = true;

	ret = devm_gpiochip_add_data(dev, gc, pi4io16);
	if (ret < 0)
		dev_err(dev, "devm_gpiochip_add_data failed: %i", ret);
	return ret;
}

static irqreturn_t pi4io16_irq_handler(int irq, void *dev_id)
{
	struct pi4io16_data *pi4io16;
	int ret;
	u16 status;
	u16 input;

	pi4io16 = dev_id;

	ret = pi4io16_i2c_read_word(pi4io16->client,
				    PI4IO16_REG_INTERRUPT_STATUS_0, &status);
	if (ret < 0)
		goto out;

	ret = pi4io16_i2c_read_word(pi4io16->client, PI4IO16_REG_INPUT_0,
				    &input);
	if (ret < 0)
		goto out;

	for (unsigned int i = 0; i < pi4io16->gpio.ngpio; ++i) {
		u16 mask;
		bool was_triggered_high, was_triggered_low;

		mask = BIT(i);
		if (!(status & mask))
			continue;

		was_triggered_high = (mask & pi4io16->irq_rising_triggers) &&
				     (mask & input);

		was_triggered_low = (mask & pi4io16->irq_falling_triggers) &&
				    !(mask & input);

		if (was_triggered_high || was_triggered_low)
			handle_nested_irq(
				irq_find_mapping(pi4io16->gpio.irq.domain, i));
	}

out:
	return IRQ_HANDLED;
}

static int pi4io16_irq_init(struct pi4io16_data *pi4io16)
{
	int ret;

	pi4io16->irq_pin =
		gpiod_get(&pi4io16->client->dev, "pi4ioe5v6416irq", GPIOD_ASIS);
	if (IS_ERR(pi4io16->irq_pin)) {
		ret = PTR_ERR(pi4io16->irq_pin);
		pi4io16->irq_pin = NULL;
		dev_err(&pi4io16->client->dev, "failed to acquire irq pin: %i",
			ret);
		goto out;
	}

	ret = gpiod_to_irq(pi4io16->irq_pin);
	if (ret < 0) {
		dev_err(&pi4io16->client->dev,
			"failed to get irq from gpiod: %i", ret);
		goto out;
	}

	pi4io16->irq = (unsigned int)ret;

	ret = request_threaded_irq(pi4io16->irq, NULL, pi4io16_irq_handler,
				   IRQF_TRIGGER_FALLING | IRQF_ONESHOT,
				   PI4IO16_IRQ_NAME, pi4io16);
out:
	if (ret < 0 && pi4io16->irq_pin) {
		gpiod_put(pi4io16->irq_pin);
		pi4io16->irq_pin = NULL;
	}

	return ret;
}

static int pi4io16_dump_stats(struct seq_file *m, void *v)
{
	int ret;
	u16 cfg_reg;
	u16 pull_down_up_enable_reg;
	u16 pull_down_up_select_reg;

	struct pi4io16_data *pi4io16 = m->private;

	mutex_lock(&pi4io16->lock);

	ret = pi4io16_i2c_read_word(pi4io16->client,
				    PI4IO16_REG_CONFIGURATION_0, &cfg_reg);
	if (ret >= 0)
		seq_printf(m, "configuration: %04x\n", cfg_reg);
	else
		seq_printf(m, "configuration: error\n");

	ret = pi4io16_i2c_read_word(pi4io16->client,
				    PI4IO16_REG_PULL_UP_PULL_DOWN_ENABLE_0,
				    &pull_down_up_enable_reg);
	if (ret >= 0)
		seq_printf(m, "pull up / pull down: %04x\n",
			   pull_down_up_enable_reg);
	else
		seq_printf(m, "pull up / pull down: error\n");

	ret = pi4io16_i2c_read_word(pi4io16->client,
				    PI4IO16_REG_PULL_UP_PULL_DOWN_SELECT_0,
				    &pull_down_up_select_reg);
	if (ret >= 0)
		seq_printf(m, "pull up / pull down select: %04x\n",
			   pull_down_up_select_reg);
	else
		seq_printf(m, "pull up / pull down select: error\n");

	mutex_unlock(&pi4io16->lock);
	return 0;
}

static int pi4io16_proc_open(struct inode *inode, struct file *file)
{
	return single_open(file, pi4io16_dump_stats, pde_data(inode));
}

static const struct proc_ops pi4io16_proc_ops = {
	.proc_open = pi4io16_proc_open,
	.proc_read = seq_read,
	.proc_lseek = seq_lseek,
	.proc_release = single_release,
};

static int pi4io16_reset_device_registers(struct pi4io16_data *pi4io16)
{
	int ret;

	/* NOTE(José): Set all pins to input */
	ret = pi4io16_i2c_write_word_and_verify(
		pi4io16->client, PI4IO16_REG_CONFIGURATION_0, 0xffff);
	if (ret < 0)
		goto out;

	/* NOTE(José): Disable latch on pins */
	ret = pi4io16_i2c_write_word_and_verify(
		pi4io16->client, PI4IO16_REG_INPUT_LATCH_0, 0x0000);
	if (ret < 0)
		goto out;

	/* NOTE(José): Disable pull-up / pull-down */
	ret = pi4io16_i2c_write_word_and_verify(
		pi4io16->client, PI4IO16_REG_PULL_UP_PULL_DOWN_ENABLE_0,
		0x0000);
	if (ret < 0)
		goto out;

	/* NOTE(José): Mask all interrupts */
	pi4io16->irq_mask_reg = 0xffff;
	pi4io16->cur_irq_mask_reg = pi4io16->irq_mask_reg;
	ret = pi4io16_i2c_write_word_and_verify(pi4io16->client,
						PI4IO16_REG_INTERRUPT_MASK_0,
						pi4io16->irq_mask_reg);
	if (ret < 0)
		goto out;

out:
	return ret;
}

static int pi4io16_i2c_probe(struct i2c_client *client)
{
	struct pi4io16_data *pi4io16;
	int ret;

	dev_info(&client->dev, "pi4io16_i2c_probe()");

	ret = i2c_check_functionality(client->adapter, I2C_FUNC_I2C);
	if (!ret) {
		dev_err(&client->dev, "i2c_check_functionality() failed: %i",
			ret);
		return -EIO;
	}

	pi4io16 = devm_kzalloc(&client->dev, sizeof(struct pi4io16_data),
			       GFP_KERNEL);
	if (!pi4io16) {
		dev_err(&client->dev, "devm_kzalloc() failed");
		return -ENOMEM;
	}

	i2c_set_clientdata(client, pi4io16);
	pi4io16->client = client;

	ret = pi4io16_reset_device_registers(pi4io16);
	if (ret < 0) {
		dev_err(&client->dev,
			"pi4io16_reset_device_registers() failed: %i", ret);
		return -EIO;
	}

	ret = pi4io16_irq_init(pi4io16);
	if (ret < 0) {
		dev_err(&client->dev, "pi4io16_irq_init() failed: %i", ret);
		return ret;
	}

	mutex_init(&pi4io16->lock);
	mutex_init(&pi4io16->irq_lock);

	ret = pi4io16_gpio_setup(pi4io16);
	if (ret < 0) {
		/* TODO(José): Release IRQ. */
		dev_err(&client->dev, "pi4io16_gpio_setup() failed: %i", ret);
		return ret;
	}

	pi4io16->proc_file = proc_create_data(PI4IO16_DRV_NAME, 0444, NULL,
					      &pi4io16_proc_ops, pi4io16);
	if (!pi4io16->proc_file) {
		pr_info(PI4IO16_DRV_NAME ": could not initialize /proc/%s",
			PI4IO16_DRV_NAME);
		return -ENOMEM;
	}

	return ret;
}

static const struct of_device_id pi4io16_dt_ids[] = {
	{
		.compatible = "pericom," PI4IO16_DRV_NAME,
	},
	{},
};
MODULE_DEVICE_TABLE(of, pi4io16_dt_ids);

static struct i2c_driver pi4io16_i2c_driver = {
	.driver = { .name = PI4IO16_DRV_NAME, .of_match_table = pi4io16_dt_ids, .owner = THIS_MODULE, },
	.probe = pi4io16_i2c_probe,
};

module_i2c_driver(pi4io16_i2c_driver);

MODULE_DESCRIPTION("PI4IOE5V6416 driver");
MODULE_LICENSE("GPL");
MODULE_AUTHOR("José Guilherme <jose.guilherme.cr.bh@proton.me>");
