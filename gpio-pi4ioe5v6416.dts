/* SPDX-License-Identifier: GPL-2.0 */

/*
 * gpio-pi4ioe5v6416.dts - Device Tree file for PI4IOE5V6416 IO expander.
 *
 */

/dts-v1/;
/plugin/;

/{
	compatible = "brcm,bcm2711";

	fragment@0 {
		target = <&gpio>;
		__overlay__ {
			pi4ioe5v6416irq: pi4ioe5v6416irq {
				brcm,pins = <25>;
				brcm,pull = <2>; /* pull up */
				brcm,function = <0>; /* in */
			};
		};
	};

	fragment@1 {
		target = <&i2c4>;
		__overlay__ {
			#address-cells = <1>;
			#size-cells = <0>;

			clock-frequency = <400000>;

			status = "okay";

			pi4ioe5v6416@0 {
				compatible = "pericom,pi4ioe5v6416";

				#gpio-cells = <2>;
				gpio-controller;

				#interrupt-cells = <2>;
				interrupt-controller;

				/*
				 * This value actually depends on the hardware setup.
				 * It can also be 0x20.
				 */
				reg = <0x21>;

				pinctrl-names = "default";
				pinctrl-0 = <&pi4ioe5v6416irq>;

				pi4ioe5v6416irq-gpios = <&gpio 25 0>;

				interrupt-parent = <&gpio>;
				interrupts = <25 0x2>;

				status = "okay";
			};
		};
	};

	fragment@2 {
		target = <&i2c4_pins>;
		__overlay__ {
			brcm,pins = <6 7>;
		};
	};
};
