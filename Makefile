obj-m += src/gpio-pi4ioe5v6416.o

KDIR ?= /lib/modules/$(shell uname -r)/build

all:
	make -C $(KDIR) M=$(PWD) modules

modules_install:
	make -C $(KDIR) M=$(PWD) modules_install

dtbo:
	dtc -@ -I dts -O dtb -o gpio-pi4ioe5v6416.dtbo gpio-pi4ioe5v6416.dts

clean:
	make -C $(KDIR) M=$(PWD) clean
